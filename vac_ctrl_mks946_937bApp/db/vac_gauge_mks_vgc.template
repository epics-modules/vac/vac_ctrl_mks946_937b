################################################################################
# Database for cold cathode gauge for
# MKS946 Vacuum Mass Flow & Gauge Controller or
# MKS937B Gauge Controller
################################################################################

record("*", "${P}${R}#EGUChRec") {
    field(LNK2, "${P}${R}ProtSP-RB.EGU PP")

    field(LNK3, "${P}${R}ProtSPS.EGU NPP")

    field(LNK4, "${P}${R}CtrlSP-RB.EGU PP")

    field(LNK5, "${P}${R}CtrlSPS.EGU NPP")

    field(LNK6, "${P}${R}CtrlSPHyst-RB.EGU PP")

    field(LNK7, "${P}${R}CtrlSPHystS.EGU NPP")

    field(LNK8, "${P}${R}FastRlySP-RB.EGU PP")

    field(LNK9, "${P}${R}FastRlySPS.EGU NPP")
}


record("*", "${P}${R}#CheckType") {
    field(CALC, "(A&&AA=='CC')?'1':'0'")
}


record(ai, "${P}${R}ProtSP-RB") {
    field(DESC, "Get protection setpoint")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_protection_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "mbar")
    field(PREC, "2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}ProtSPS") {
    field(DESC, "Set protection setpoint")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_protection_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")
    field(FLNK, "${P}${R}ProtSP-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(fanout, "${P}${R}#CtrlReread-FO") {
    field(LNK1, "${P}${R}CtrlSP-RB")
    field(LNK2, "${P}${R}CtrlSPHyst-RB")
    field(LNK3, "${P}${R}CtrlSPExtRng-RB")

    field(SCAN, "Event")
    field(EVNT, "${P}-RB")
}


record(ai, "${P}${R}CtrlSP-RB") {
    field(DESC, "Get control setpoint")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_control_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}CtrlSPS") {
    field(DESC, "Set control setpoint")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_control_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")
    field(FLNK, "${P}${R}#CtrlReread-FO.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(bi, "${P}${R}CtrlSPExtRng-RB") {
    field(DESC, "Get control setpoint extended range")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_control_setpoint_er(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZNAM, "OFF")
    field(ONAM, "ON")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bo, "${P}${R}CtrlSPExtRngS") {
    field(DESC, "Set control setpoint extended range")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_control_setpoint_er(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(FLNK, "${P}${R}#CtrlReread-FO.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(ai, "${P}${R}CtrlSPHyst-RB") {
    field(DESC, "Get control setpoint hysteresis")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_control_setpoint_hyst(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}

record(ao, "${P}${R}CtrlSPHystS") {
    field(DESC, "Set control setpoint hysteresis")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_control_setpoint_hyst(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")
    field(FLNK, "${P}${R}#CtrlReread-FO.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(mbbi, "${P}${R}CtrlChan-RB") {
    field(DESC, "Get control channel")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_control_channel(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "A1")

    field(TWVL, "2")
    field(TWST, "A2")

    field(THVL, "3")
    field(THST, "B1")

    field(FRVL, "4")
    field(FRST, "B2")

    field(FVVL, "5")
    field(FVST, "C1")

    field(SXVL, "6")
    field(SXST, "C2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}CtrlChanS") {
    field(DESC, "Set control channel")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_control_channel(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "A1")

    field(TWVL, "2")
    field(TWST, "A2")

    field(THVL, "3")
    field(THST, "B1")

    field(FRVL, "4")
    field(FRST, "B2")

    field(FVVL, "5")
    field(FVST, "C1")

    field(SXVL, "6")
    field(SXST, "C2")

    field(FLNK, "${P}${R}CtrlChan-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(mbbi, "${P}${R}CtrlMode-RB") {
    field(DESC, "Get control mode status")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_control_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "AUTO")

    field(TWVL, "2")
    field(TWST, "SAFE")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}CtrlModeS") {
    field(DESC, "Set control mode status")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_control_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "AUTO")

    field(TWVL, "2")
    field(TWST, "SAFE")

    field(FLNK, "${P}${R}CtrlMode-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(ai, "${P}${R}GasCorrF-RB") {
    field(DESC, "Get control gas correction factor")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_gas_correction_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}GasCorrFS") {
    field(DESC, "Set control gas correction factor")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_gas_correction_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(PREC, "1")
    field(DRVL, "0.1")
    field(LOPR, "0.1")
    field(DRVH, "10")
    field(HOPR, "10")
    field(FLNK, "${P}${R}GasCorrF-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(bi, "${P}${R}Power-RB") {
    field(DESC, "Get power status")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_power_status(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-2")

    field(ZNAM, "OFF")
    field(ONAM, "ON")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bo, "${P}${R}PowerS") {
    field(DESC, "Set power status")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_power_status(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(FLNK, "${P}${R}Power-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbi, "${P}${R}GasType-RB") {
    field(DESC, "Get gas type")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_gas_type(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "Nitrogen")

    field(ONVL, "1")
    field(ONST, "Argon")

    field(TWVL, "2")
    field(TWST, "Helium")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}GasTypeS") {
    field(DESC, "Set gas type")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_gas_type(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "Nitrogen")

    field(ONVL, "1")
    field(ONST, "Argon")

    field(TWVL, "2")
    field(TWST, "Helium")

    field(FLNK, "${P}${R}GasType-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(mbbi, "${P}${R}GaugeStatR") {
    field(DESC, "Get gauge status")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_gauge_status(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-1")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "WAIT")

    field(TWVL, "2")
    field(TWST, "GOOD")

    field(THVL, "3")
    field(THST, "PROTECT")

    field(FRVL, "4")
    field(FRST, "Control")

    field(FVVL, "5")
    field(FVST, "Rear Panel Ctrl off")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ai, "${P}${R}StartupDly-RB") {
    field(DESC, "Get startup delay")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_startup_delay(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "sec")
    field(PREC, "0")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}StartupDlyS") {
    field(DESC, "Set startup delay")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_startup_delay(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "sec")
    field(PREC, "0")
    field(DRVL, "3")
    field(LOPR, "3")
    field(DRVH, "300")
    field(HOPR, "300")
    field(FLNK, "${P}${R}StartupDly-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(ai, "${P}${R}FastRlySP-RB") {
    field(DESC, "Get fast relay control setpoint")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgc.proto get_frc_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "mbar")
    field(PREC, "2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}FastRlySPS") {
    field(DESC, "Set fast relay control setpoint")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgc.proto set_frc_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "mbar")
    field(PREC, "2")
    field(FLNK, "${P}${R}FastRlySP-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}

