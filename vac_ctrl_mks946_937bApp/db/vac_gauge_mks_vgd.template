################################################################################
# Database for capacitance manometer gauge for
# MKS946 Vacuum Mass Flow & Gauge Controller or
# MKS937B Gauge Controller
################################################################################

record("*", "${P}${R}#EGUChRec") {
    field(LNK2, "${P}${R}PrsRng-RB.EGU PP")

    field(LNK3, "${P}${R}PrsRngS.EGU NPP")
}


record("*", "${P}${R}#CheckType") {
    field(CALC, "(A&&AA=='CM')?'1':'0'")
}


record(ai, "${P}${R}PrsRng-RB") {
    field(DESC, "Full scale pressure measurement range")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgd.proto get_pressure_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "mbar")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}PrsRngS") {
    field(DESC, "Full scale pressure measurement range")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgd.proto set_pressure_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

#The first value is in Torr, the second one is in mBar
#Need to verify if UNIT changes affect this one
    field(DRVH, "20000")
    field(DRVH, "26665")
    field(DRVL, "0.01")
    field(HOPR, "20000")
    field(HOPR, "26665")
    field(LOPR, "0.01")
    field(EGU,  "mbar")
    field(FLNK, "${P}${R}PrsRng-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(bi, "${P}${R}Type-RB") {
    field(DESC, "Get capacitance manometer type")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgd.proto get_type(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZNAM, "Absolute")
    field(ONAM, "Differential")
    field(FLNK, "${P}${R}#FixVRng")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bo, "${P}${R}TypeS") {
    field(DESC, "Set capacitance manometer type")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgd.proto set_type(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZNAM, "Absolute")
    field(ONAM, "Differential")
    field(FLNK, "${P}${R}Type-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(mbbi, "${P}${R}VoltageRng-RB") {
    field(DESC, "Get full scale voltage")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgd.proto get_voltage_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "1V")

    field(ONVL, "1")
    field(ONST, "5V")

    field(TWVL, "2")
    field(TWST, "10V")

    field(THVL, "3")
    field(THST, "1V (uni)")

    field(FRVL, "4")
    field(FRST, "5V (uni)")

    field(FVVL, "5")
    field(FVST, "10V (uni)")

    field(SXVL, "6")
    field(SXST, "1V (bi)")

    field(SVVL, "7")
    field(SVST, "5V (bi)")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}VoltageRngS") {
    field(DESC, "Set full scale voltage")
    field(DTYP, "Raw Soft Channel")
    field(OUT,  "${P}${R}#VoltageRngS PP")

    field(ZRVL, "0")
    field(ZRST, "1V")

    field(ONVL, "1")
    field(ONST, "5V")

    field(TWVL, "2")
    field(TWST, "10V")

    field(THVL, "3")
    field(THST, "1V (uni)")

    field(FRVL, "4")
    field(FRST, "5V (uni)")

    field(FVVL, "5")
    field(FVST, "10V (uni)")

    field(SXVL, "6")
    field(SXST, "1V (bi)")

    field(SVVL, "7")
    field(SVST, "5V (bi)")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


record(longout, "${P}${R}#VoltageRngS") {
    field(DESC, "Set full scale voltage")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgd.proto set_voltage_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(FLNK, "${P}${R}VoltageRng-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bo, "${P}${R}ZeroS") {
    field(DESC, "Zero the gauge")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgd.proto set_zero(${SERADDR},${CHANNEL_N},${P}${R}Zero-RB) ${ASYNPORT}")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bi, "${P}${R}Zero-RB") {
    field(DESC, "Result of Zero operation")
    field(ZNAM, "Failed")
    field(ONAM, "OK")
}


record(mbbi, "${P}${R}AutozeroChan-RB") {
    field(DESC, "Get autozero control channel")
    field(DTYP, "stream")
    field(INP,  "@vac_gauge_mks_vgd.proto get_autozero_channel(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "NA")

    field(ONVL, "1")
    field(ONST, "A1")

    field(TWVL, "2")
    field(TWST, "A2")

    field(THVL, "3")
    field(THST, "B1")

    field(FRVL, "4")
    field(FRST, "B2")

    field(FVVL, "5")
    field(FVST, "C1")

    field(SXVL, "6")
    field(SXST, "C2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}AutozeroChanS") {
    field(DESC, "Set autozero control channel")
    field(DTYP, "stream")
    field(OUT,  "@vac_gauge_mks_vgd.proto set_autozero_channel(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "NA")

    field(ONVL, "1")
    field(ONST, "A1")

    field(TWVL, "2")
    field(TWST, "A2")

    field(THVL, "3")
    field(THST, "B1")

    field(FRVL, "4")
    field(FRST, "B2")

    field(FVVL, "5")
    field(FVST, "C1")

    field(SXVL, "6")
    field(SXST, "C2")

    field(FLNK, "${P}${R}AutozeroChan-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")

    info(autosaveFields_pass0, "VAL")
}


#The Voltage ranges are different depending on the CM type
record(seq, "${P}${R}#FixVRng") {
    field(DOL1, "1")
    field(LNK1, "${P}${R}#AbsVRng.PROC")

    field(DOL2, "1")
    field(LNK2, "${P}${R}#DiffVRng.PROC")

    field(OFFS, "1")
    field(SELL, "${P}${R}Type-RB")
    field(SELM, "Specified")
}


record(stringin, "${P}${R}#EmptyStr") {
    field(DISP, "1")
    field(PINI, "YES")
    field(VAL, "")
}


record(sseq, "${P}${R}#AbsVRng") {
    field(STR1, "1V")
    field(LNK1, "${P}${R}VoltageRngS.ZRST")

    field(STR2, "5V")
    field(LNK2, "${P}${R}VoltageRngS.ONST")

    field(STR3, "10V")
    field(LNK3, "${P}${R}VoltageRngS.TWST")

    field(DOL4, "${P}${R}#EmptyStr")
    field(LNK4, "${P}${R}VoltageRngS.THST")

    field(DOL5, "${P}${R}#EmptyStr")
    field(LNK5, "${P}${R}VoltageRngS.FRST")

    field(DOL6, "0")
    field(LNK6, "${P}${R}VoltageRngS.ZRVL")

    field(DOL7, "1")
    field(LNK7, "${P}${R}VoltageRngS.ONVL")

    field(DOL8, "2")
    field(LNK8, "${P}${R}VoltageRngS.TWVL")

    field(DOL9, "0")
    field(LNK9, "${P}${R}VoltageRngS.THVL")

    field(DOLA, "0")
    field(LNKA, "${P}${R}VoltageRngS.FRVL")
}


record(sseq, "${P}${R}#DiffVRng") {
    field(STR1, "1V (uni)")
    field(LNK1, "${P}${R}VoltageRngS.ZRST")

    field(STR2, "5V (uni)")
    field(LNK2, "${P}${R}VoltageRngS.ONST")

    field(STR3, "10V (uni)")
    field(LNK3, "${P}${R}VoltageRngS.TWST")

    field(STR4, "1V (bi)")
    field(LNK4, "${P}${R}VoltageRngS.THST")

    field(STR5, "5V (bi)")
    field(LNK5, "${P}${R}VoltageRngS.FRST")

    field(DOL6, "3")
    field(LNK6, "${P}${R}VoltageRngS.ZRVL")

    field(DOL7, "4")
    field(LNK7, "${P}${R}VoltageRngS.ONVL")

    field(DOL8, "5")
    field(LNK8, "${P}${R}VoltageRngS.TWVL")

    field(DOL9, "6")
    field(LNK9, "${P}${R}VoltageRngS.THVL")

    field(DOLA, "7")
    field(LNKA, "${P}${R}VoltageRngS.FRVL")
}

