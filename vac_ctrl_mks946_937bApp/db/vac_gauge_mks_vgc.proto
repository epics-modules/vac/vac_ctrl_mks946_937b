################################################################################
# Protocol file for cold cathode gauge for
# MKS946 Vacuum Mass Flow & Gauge Controller or
# MKS937B Gauge Controller
################################################################################

Terminator  = ";FF";

ReadTimeout   = 1000;
WriteTimeout  = 700;
PollPeriod    = 100;
ReplyTimeout  = 1000;
LockTimeout   = 30000;

ExtraInput    = Ignore;

start_comm    = "@254";
ack_resp      = "@%*dACK";
nak_resp      = "@%*dNAK";
start_comm    = "@%(\$1)03d";
ack_resp      = "@%(\$1)=03dACK";
nak_resp      = "@%(\$1)=03dNAK";

channel       = "%(\$2)d";
on_off        = "%{OFF|ON}";

reconnect {
    disconnect;
    wait 5000;
    connect 1000;
}
@replytimeout {
    reconnect;
}

################################################################################
################################################################################
#
# Cold Cathode
#
################################################################################
################################################################################

################################################################################
# Get protection setpoint
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_protection_setpoint {
    out $start_comm "PRO" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $ack_resp "%{DISABLE}";
    }
}

################################################################################
# Set protection setpoint
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_protection_setpoint {
    out $start_comm "PRO" $channel "!%f";
    in  $ack_resp   "%f";

    @mismatch {
        in $ack_resp "%{DISABLE}";
    }
}

################################################################################
# Disable protection setpoint
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
disable_protection_setpoint {
    out $start_comm "PRO" $channel "!DISABLE";
    in  $ack_resp   "DISABLE";
}

################################################################################
# Get control setpoint
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_control_setpoint {
    out $start_comm "CSP" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Set control setpoint
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_control_setpoint {
    out $start_comm "CSP" $channel "!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Query extended control setpoint range status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_control_setpoint_er {
    out $start_comm "XCS" $channel "?";
    in  $ack_resp   $on_off;

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Change extended control setpoint range state
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_control_setpoint_er {
    out $start_comm "XCS" $channel "!" $on_off;
    in  $ack_resp                      $on_off;
}

################################################################################
# Get control setpoint hysteresis
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_control_setpoint_hyst {
    out $start_comm "CHP" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Set control setpoint hysteresis
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_control_setpoint_hyst {
    out $start_comm "CHP" $channel "!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Get control channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
control_channels = "%{OFF|A1|A2|B1|B2|C1|C2}";
get_control_channel {
    out $start_comm "CSE" $channel "?";
    in  $ack_resp   $control_channels;

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Set control channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_control_channel {
    out $start_comm "CSE" $channel "!" $control_channels;
    in  $ack_resp                      $control_channels;
}

################################################################################
# Get control mode
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
control_modes = "%{OFF|AUTO|SAFE}";
get_control_mode {
    out $start_comm "CTL" $channel "?";
    in  $ack_resp   $control_modes;
}

################################################################################
# Set control mode
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_control_mode {
    out $start_comm "CTL" $channel "!" $control_modes;
    in  $ack_resp                      $control_modes;

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Get gas correction factor
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_gas_correction_factor {
    out $start_comm "UC" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Set gas correction factor
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_gas_correction_factor {
    out $start_comm "UC" $channel "!%f";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "152";
    }
}

################################################################################
# Get channel power status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_power_status {
    out $start_comm "CP" $channel "?";
    in  $ack_resp   $on_off;
}

################################################################################
# Set channel power status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_power_status {
    out $start_comm "CP" $channel "!" $on_off;
    in  $ack_resp                     $on_off;
}

################################################################################
# Get gas type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
gas_types = "%{NITROGEN|ARGON|HELIUM}";
get_gas_type {
    out $start_comm "GT" $channel "?";
    in  $ack_resp   $gas_types;
}

################################################################################
# Set gas type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_gas_type {
    out $start_comm "GT" $channel "!" $gas_types;
    in  $ack_resp                     $gas_types;
}

################################################################################
# Get gauge status
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_gauge_status {
    out $start_comm "T" $channel "?";
    in  $ack_resp   "%{O|W|G|P|C|R}";
}

################################################################################
# Get startup delay
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_startup_delay {
    out $start_comm "TDC" $channel "?";
    in  $ack_resp   "%d";
}

################################################################################
# Set startup delay
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_startup_delay {
    out $start_comm "TDC" $channel "!%d";
    in  $ack_resp   "%d";
}

################################################################################
# Get pressure setpoint for fast relay trigger
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_frc_setpoint {
    out $start_comm "FRC" $channel "?";
    in  $ack_resp   "%f";
}

################################################################################
# Set pressure setpoint for fast relay trigger
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_frc_setpoint {
    out $start_comm "FRC" $channel "!%f";
    in  $ack_resp   "%f";
}
