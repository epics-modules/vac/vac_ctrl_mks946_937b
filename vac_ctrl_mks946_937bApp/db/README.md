# db files for MKS vacuum gauge controller and its gauges

The pressure values and relay statuses are read in bulk:
*   one request to read pressure on all gauges
*   one request to return relay status of all relays

This information is then separated into individual PVs of the controller:
*   `$(CONTROLLERNAME)A1_PrsR`
*   `$(CONTROLLERNAME)A2_PrsR`
*    ...
*   `$(CONTROLLERNAME)C2_PrsR`

However, we also need to provide this information using the gauge names. The easiest solution would to simply create aliases for guages.
The problem with this is that the archiver appliance will resolve the alias and use the "parent" controller PV name; it can be a problem if
the gauge is later moved to a different channel or different controller altogether. So the solution we use is that we create dedicated pressure
PVs for the gauges (but not yet(?) for the relays, the relay PVs are not archived) and use `INP` links to get the values from the controller PVs

## vac_gauge_mks_common.template

Common PVs for all the gauges:
*   CtrlNameR
    *   ESS name of gauge controller
    *   stringin / constant
*   ModuleNameR
    *   Slot of the measurement board (A, B, C)
    *   stringin / constant
*   SensorTypeR
    *   Two letter typecode of the sensor (CC, CM, CP, PR, FC)
    *   stringin
*   CommsOK
    *   Whether communication is okay with device
    *   bi
*   CtrlUITypeR
    *   Type of controller; initialized to `MKS9xx` (for OPI)
    *   stringin / constant
*   ChanR
    *   The channel this gauge is connected to
    *   stringin / constant
*   ModuleValidR
    *   The measurement board serial number is the same as configured
    *   bi
*   ValidR
    *   Communication is okay with device and gauge is connected / correctly configured
    *   bi
*   NumOfRlysR
    *   Number of relays assigned to gauge (2 or 4)
    *   longin
*   RlyValidR
    *   Relay configuration is correct (not meaningful for MKS but OPI expects it)
    *   bi
*   PrsR-STR
    *   The pressure value as read from the gauge
    *   stringin
*   PrsR
    *   The pressure value
    *   ai
*   PrsStatR
    *   Pressure / gauge status
    *   mbbi
        *   `INVALID`
        *   `ON`
        *   `OFF`
        *   `ERROR`
        *   `OVER-RANGE`
        *   `UNDER-RANGE`

## vac_gauge_mks_vgc.template

PVs for the Cold cathode gauge:
*   ProtSP-RB
    *   Protection setpoint readback
    *   ai
*   ProtSPS
    *   Protection setpoint control
    *   ao
*   CtrlSP-RB
    *   Control setpoint readback
    *   ai
*   CtrlSPS
    *   Control setpoint control
    *   ao
*   CtrlSPExtRng-RB
    *   Control setpoint extended range status reaback
    *   bi
*   CtrlSPExtRngS
    *   Control setpoint extended range control
    *   bo
*   CtrlSPHyst-RB
    *   Control setpoint hysteresis readback
    *   ai
*   CtrlSPHystS
    *   Control setpoint hysteresis control
    *   ao
*   CtrlChan-RB
    *   Control channel (A1, A2, ..., C2) assigned to gauge readback
    *   mbbi
*   CtrlChanS
    *   Control channel (A1, A2, ..., C2) assigned to gauge control
    *   mbbo
*   CtrlMode-RB
    *   Control mode state readback
    *   mbbi
        *   `OFF`
        *   `AUTO`
        *   `SAFE`
*   CtrlModeS
    *   Control mode state control
    *   mbbo
        *   `OFF`
        *   `AUTO`
        *   `SAFE`
*   GasCorrF-RB
    *   Gas correction factor readback
    *   ai
*   GasCorrFS
    *   Gas correction factor control
    *   ao
*   Power-RB
    *   Power status readback
    *   bi
        *   `ON`
        *   `OFF`
*   PowerS
    *   Power status control
    *   bo
        *   `ON`
        *   `OFF`
*   GasType-RB
    *   Gas type readback
    *   mbbi
        *   `Nitrogen`
        *   `Argon`
        *   `Helium`
*   GasTypeS
    *   Gas type control
    *   mbbo
        *   `Nitrogen`
        *   `Argon`
        *   `Helium`
*   GaugeStatR
    *   Gauge status
    *   mbbi
        *   `OFF`
        *   `WAIT`
        *   `GOOD`
        *   `PROTECT`
        *   `Control`
        *   `Rear Panel Ctrl Off`
*   StartupDly-RB
    *   Startup delay readback
    *   ai
*   StartupDlyS
    *   Startup delay control
    *   ao
*   FastRlySP-RB
    *   Fast relay control setpoint readback
    *   ai
*   FastRlySPS
    *   Fast relay control setpoint control
    *   ao

## vac_gauge_mks_vgd.template

PVs for the Capacitance gauge:
*   PrsRng-RB
    *   Full scale pressure measurement range readback
    *   ai
*   PrsRngS
    *   Full scale pressure measurement range control
    *   ao
*   Type-RB
    *   Capacitane manometer type readback
    *   bi
        *   `Absolute`
        *   `Differential`
*   TypeS
    *   Capacitane manometer type control
    *   bo
        *   `Absolute`
        *   `Differential`
*   VoltageRng-RB
    *   Full scale voltage range readback
    *   mbbi
        *   `1V`
        *   `5V`
        *   `10V`
        *   `1V (uni)`
        *   `5V (uni)`
        *   `10V (uni)`
        *   `1V (bi)`
        *   `5V (bi)`
*   VoltageRngS
    *   Full scale voltage range control
    *   mbbo
        *   `1V`
        *   `5V`
        *   `10V`
        *   `1V (uni)`
        *   `5V (uni)`
        *   `10V (uni)`
        *   `1V (bi)`
        *   `5V (bi)`
*   ZeroS
    *   Zero the gauge
    *   bo
*   AutozeroChan-RB
    *   Autozero control channel (A1, A2, ..., C2) readback
    *   mbbi
*   AutozeroChanS
    *   Autozero control channel (A1, A2, ..., C2) control
    *   mbbo

## vac_gauge_mks_vgp.template

PVs for the Pirani gauge:
*   ATMCalibPrs-RB
    *   Atmospheric pressure for calibration readback
    *   ai
*   ATMCalibPrsS
    *   Atmospheric pressure for calibration control
    *   ao
*   ZeroS
    *   Zero the gauge
    *   bo
*   AutozeroChan-RB
    *   Autozero control channel (A1, A2, ..., C2) readback
    *   mbbi
*   AutozeroChanS
    *   Autozero control channel (A1, A2, ..., C2) control
    *   mbbo
*   GasType-RB
    *   Gas type readback
    *   mbbi
        *   `Nitrogen`
        *   `Argon`
        *   `Helium`
*   GasTypeS
    *   Gas type control
    *   mbbo
        *   `Nitrogen`
        *   `Argon`
        *   `Helium`
*   Power-RB
    *   Power status readback
    *   bi
        *   `ON`
        *   `OFF`
*   PowerS
    *   Power status control
    *   bo
        *   `ON`
        *   `OFF`
*   RstCalibS
    *   Reset user calibration to default
    *   bo

## vac_mfc_mks_gv50a.template

PVs for the Mass flow controller:
*   FlwR-STR
    *   The flow rate value as read from the gauge
    *   stringin
*   FlwR
    *   The flow rate value
    *   ai
*   FlwStatR
    *   Flow / gauge status
    *   mbbi
        *   `Invalid`
        *   `Open`
        *   `Closed`
        *   `Error`
*   ScaleF-RB
    *   Scale factor readback
    *   ai
*   ScaleFS
    *   Scale factor control
    *   ao
*   NomRng-RB
    *   Full scale nominal range readback
    *   ai
*   NomRngS
    *   Full scale nominal range control
    *   ao
*   ZeroS
    *   Zero the gauge
    *   bo
*   FlwSP-RB
    *   Flow setpoint readback
    *   ai
*   FlwSPS
    *   Flow setpoint control
    *   ao
*   FlwInRngThrS
    *   The threshold in % of the flow rate should reach relative to the setpoint
    *   longout
*   FlwInRngTimeoS
    *   Time (in sec) to reach `FlwInRngThrS`% of `FlwSP-RB`
    *   longout
*   FlwSPTimElapsedR
    *   Time (in sec) it took to reach `FlwInRngThrS`% of `FlwSP-RB`
    *   longin
*   FlwInRngR
    *   Whether flow rate is within range of `FlwSP-RB` (difference is less than `FlwInRngThrS`%)
    *   bi
        *   `Out of range`
        *   `In range`
*   FlwRngTimeoR
    *   Whether flow rate timed out or not reaching range in specified time
    *   bi
*   Mode-RB
    *   Mode control readback
    *   mbbi
        *   `Closed`
        *   `Setpoint`
        *   `Open`
        *   `PID Ctrl`
        *   `Ratio A`
        *   `Ratio M`
*   ModeS
    *   Mode control control
    *   mbbo
        *   `Closed`
        *   `Setpoint`
        *   `Open`
