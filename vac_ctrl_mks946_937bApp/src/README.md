# State machine to check operation of mass flow controller

It checks if the flow rate has reached x% of the specified setpoint in the specified amount of time. Some of the actual checks are done in vac_mfc_mks_gv50a.template
