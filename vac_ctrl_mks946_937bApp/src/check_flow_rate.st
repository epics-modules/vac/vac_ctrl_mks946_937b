program check_flow_rate("P, R")

// State machine to check if the flow rate reached an acceptable level (in certain range of the specified setpoint)
// in a specified timeframe

// Make the code reentrant
option +r;

int     Mode;
assign  Mode to "{P}{R}Mode-RB";
monitor Mode;

int     SetpointMode;

evflag  ModeChanged;
sync    Mode ModeChanged;

double  Setpoint;
assign  Setpoint to "{P}{R}FlwSPS";
monitor Setpoint;

evflag  SetpointChanged;
sync    Setpoint SetpointChanged;

double  Setpoint_RB;
assign  Setpoint_RB to "{P}{R}FlwSP-RB";
monitor Setpoint_RB;

int     TimeElapsed;
assign  TimeElapsed to "{P}{R}#FlwSPTimElapsedR";
monitor TimeElapsed;

int     CountElapsedTime;
assign  CountElapsedTime to "{P}{R}#CntTimElapsed";
monitor CountElapsedTime;

int     Timeout;
assign  Timeout to "{P}{R}FlwInRngTimeoS";
monitor Timeout;

int     FlowInRange;
assign  FlowInRange to "{P}{R}FlwInRngR";
monitor FlowInRange;

int     FlowRangeTimeout;
assign  FlowRangeTimeout to "{P}{R}FlwRngTimeoR";


ss FLOW_RATE
{

	state init
	{
		entry
		{
			// It seems that they have to be cleared on init
			efTestAndClear(SetpointChanged);
			efTestAndClear(ModeChanged);

			// Integer value must be the same as in the db file
			SetpointMode = 1;
		}

		when ()
		{
		} state wait_for_sp_change
	}


	// wait for a change in flow rate setpoint or a mode change
	state wait_for_sp_change
	{
		when ((efTestAndClear(SetpointChanged) || efTestAndClear(ModeChanged)) && Mode == SetpointMode)
		{
			// Clear both because of short circuit eval of ||
			efTestAndClear(SetpointChanged);
			efTestAndClear(ModeChanged);

			FlowRangeTimeout = 0;
			pvPut(FlowRangeTimeout, ASYNC);
		} state wait_for_sp_readback
	}


	// wait until the flow rate setpoint is read back and equals to setpoint
	state wait_for_sp_readback
	{
		when (Setpoint == Setpoint_RB)
		{
			TimeElapsed = 0;
			pvPut(TimeElapsed, ASYNC);

			CountElapsedTime = 1;
			pvPut(CountElapsedTime, ASYNC);
		} state check_elapsed_time_is_zero
	}


	// wait until elapsed time counter is initialized
	state check_elapsed_time_is_zero
	{
		// bail out if not in Setpoint mode
		when (Mode != SetpointMode)
		{
		} state not_in_setpoint_mode

		// start over if setpoint is changed again
		when (efTest(SetpointChanged))
		{
		} state wait_for_sp_change

		when (pvPutComplete(TimeElapsed) && pvPutComplete(CountElapsedTime))
		{
		} state check_within_range
	}


	// check if flow rate is within range
	state check_within_range
	{
		// bail out if not in Setpoint mode
		when (Mode != SetpointMode)
		{
		} state not_in_setpoint_mode

		// start over if setpoint is changed again
		when (efTest(SetpointChanged))
		{
		} state wait_for_sp_change

		// if flow is within range, everything is fine, go wait for another setpoint change
		when (FlowInRange)
		{
			CountElapsedTime = 0;
			pvPut(CountElapsedTime, ASYNC);
		} state wait_for_sp_change

		when (TimeElapsed > Timeout)
		{
			FlowRangeTimeout = 1;
			pvPut(FlowRangeTimeout);
		} state wait_for_max_timeout
	}


	// already timed out, stop the counter when range is reached
	state wait_for_max_timeout
	{
		// bail out if not in Setpoint mode
		when (Mode != SetpointMode)
		{
		} state not_in_setpoint_mode

		// start over if setpoint is changed again
		when (efTest(SetpointChanged))
		{
		} state wait_for_sp_change

		// if flow is within range, stop elapsed time counter
		when (FlowInRange)
		{
			CountElapsedTime = 0;
			pvPut(CountElapsedTime, ASYNC);
		} state wait_for_sp_change
	}


	state not_in_setpoint_mode
	{
		entry
		{
			CountElapsedTime = 0;
			pvPut(CountElapsedTime);

			FlowRangeTimeout = 0;
			pvPut(FlowRangeTimeout);
		}

		when ()
		{
		} state wait_for_sp_change
	}
}
