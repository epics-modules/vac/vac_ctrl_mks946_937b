# Vacuum Mass Flow & Gauge Controller MKS 946, Vacuum Gauge Controller MKS 937B

EPICS module to provide communications and read/write data from/to **MKS 946 vacuum mass flow & gauge controller** and **MKS 937B vacuum gauge controller** and
*   to read/write data for **MKS vacuum cold cathode gauge**.
*   to read/write data for **MKS vacuum capacitance gauge**.
*   to read/write data for **MKS vacuum pirani gauge**.
*   to read/write data for **MKS GV50A mass flow controller (MFC)**.

The IOC does not directly communicate with the gauges; the gauge is actually connected to a vacuum controller and the IOC communicates with the controller and the controller communicates with the gauges. Controllers can have multiple gauges and MFCs connected to different channels.

**Please note that the gauges have to be configured together with the controller i.e. in the same IOC**

There are separate `.iocsh` scripts for the controller and the gauges themselves; this allows for any combination of controller and gauges to be built using this module.

## IOCSH files

*   Controller: [vac_ctrl_mks946_937b_moxa.iocsh](iocsh/README.md#vac_ctrl_mks946_937b_moxa)
*   Cold cathode gauge: [vac_gauge_mks_vgc.iocsh](iocsh/README.md#vac_gauge_mks_vgc)
*   Capacitance gague: [vac_gauge_mks_vgd.iocsh](iocsh/README.md#vac_gauge_mks_vgd)
*   Pirani gauge: [vac_gauge_mks_vgp.iocsh](iocsh/README.md#vac_gauge_mks_vgp)
*   Mass flow controller: [vac_mfc_mks_gv50a.iocsh](iocsh/README.md#vac_mfc_mks_gv50a)

## MOXA configuration

#### Operation Modes / Operating settings

```
Application: Socket
Mode: TCP Server
```

OR

```
Operation mode: TCP Server mode
```

Enable delimiter processing (the messages are terminated by `;FF`):
1.   wait for `;` (`0x3B`)
2.   followed by `F` (`0x46`)
3.   then wait for another character (`Delimiter+1`)

```
.
.
.
Packing length: 0
Delimiter 1: 3B / Enabled
Delimiter 2: 46 / Enabled
Delimiter process: Delimiter+1
Force transmit: 0
```

#### Communication Parameters / Serial Settings

The default baudrate is 9600 but recommend changing it to 115200. Of course the MOXA and device settings have to match, so you have to change it first on the device (by using the PV `SetHighSpeed`) and then on the MOXA.

```
Baud rate: 115200
Data bits: 8
Stop bits: 1
Parity: None
Flow control: None
Interface: RS-232
```

