# IOCSH files for MKS vacuum gauge controller and its gauges

## vac_ctrl_mks946_937b_moxa

`.iocsh` file for the controller.

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **IPADDR**
    *   The IP address or fully qualified domain name of the Moxa serial ethernet controller the gauge controller is connected to
    *   string
*   **PORT**
    *   The TCP port number of the serial port on the Moxa where the gauge controller is connected to (serial port 1 is usually TCP port 4001, serial port 2 is TCP port 4002, and so on)
    *   integer

#### Optional macros:

*   **BOARD_A_SERIAL_NUMBER**
    *   The serial number of measurement board A
    *   default is empty string
    *   string
*   **BOARD_B_SERIAL_NUMBER**
    *   The serial number of measurement board B
    *   default is empty string
    *   string
*   **BOARD_C_SERIAL_NUMBER**
    *   The serial number of measurement board C
    *   default is empty string
    *   string

Gauge settings are stored on the measurement board and the serial number is used to detect if a measurement board has changed and reconfiguration is necessary.


## vac_gauge_mks_vgc

`.iocsh` file for the Cold cathode gauge

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the gauge controller the gauge is connected to
    *   string
*   **CHANNEL**
    *   The channel this gauge is connected to on the controller. Valid values:
        *   A1
        *   B1
        *   C1
    *   string

#### Optional macros:

*   **RELAY1_DESC**
    *   A description of what Relay 1 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY2_DESC**
    *   A description of what Relay 2 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY3_DESC**
    *   A description of what Relay 3 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY4_DESC**
    *   A description of what Relay 4 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string


## vac_gauge_mks_vgd

`.iocsh` file for the Capacitance gauge

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the gauge controller the gauge is connected to
    *   string
*   **CHANNEL**
    *   The channel this gauge is connected to on the controller. Valid values:
        *   A1
        *   A2
        *   B1
        *   B2
        *   C1
        *   C2
    *   string

#### Optional macros:

*   **RELAY1_DESC**
    *   A description of what Relay 1 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY2_DESC**
    *   A description of what Relay 2 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string


## vac_gauge_mks_vgp

`.iocsh` file for the Pirani gauge

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the gauge controller the gauge is connected to
    *   string
*   **CHANNEL**
    *   The channel this gauge is connected to on the controller. Valid values:
        *   A1
        *   A2
        *   B1
        *   B2
        *   C1
        *   C2
    *   string

#### Optional macros:

*   **RELAY1_DESC**
    *   A description of what Relay 1 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY2_DESC**
    *   A description of what Relay 2 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string


## vac_mfc_mks_gv50a

`.iocsh` file for the Mass flow controller

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the gauge controller the gauge is connected to
    *   string
*   **CHANNEL**
    *   The channel this gauge is connected to on the controller. Valid values:
        *   A1
        *   A2
        *   B1
        *   B2
        *   C1
        *   C2
    *   string

#### Optional macros:

*   **RELAY1_DESC**
    *   A description of what Relay 1 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
*   **RELAY2_DESC**
    *   A description of what Relay 2 controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string


## vac_gauge_mks_relay_desc

`.iocsh` file to specify what a certain relay of a certain gauge controls / represents. This information can be displayed on the OPI
This `.iocsh` can useful if you want to set the relay description from a template

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **RELAY**
    *   The relay number. Valid values:
        *   1
        *   2
        *   3 (only for cold cathode gauges)
        *   4 (only for cold cathode gauges)
    *   integer
*   **RELAY_DESC**
    *   A description of what this relay controls / represents (to be displayed on the OPI)
    *   default is empty string
    *   string
